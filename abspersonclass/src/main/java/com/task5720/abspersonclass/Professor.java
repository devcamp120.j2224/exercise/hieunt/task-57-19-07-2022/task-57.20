package com.task5720.abspersonclass;

public class Professor extends Person {
	private int salary;
	// khởi tạo thiếu salary
	public Professor(int age, String gender,
	String name, Address address) {
		super(age, gender, name, address);
		this.salary = 6000;
	}
	// khởi tạo đầy đủ tham số
	public Professor(int age, String gender,
	String name, Address address, int salary) {
		super(age, gender, name, address);
		this.salary = salary;
	}
	@Override
	public void eat() {
		System.out.println("Professor is eating");
	}
	public void teaching() {
		System.out.println("Professor is teaching");
	}
	// getter setter
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
}
