package com.task5720.abspersonclass;

import java.util.ArrayList;

public class Student extends Person {
	private int studentId;
	private ArrayList<Subject> listSubject;
	// khởi tạo ko có studentId và listSubject
	public Student(int age, String gender,
	String name, Address address) {
		super(age, gender, name, address);
	}
	// khởi tạo đầy đủ tham số
	public Student(int age, String gender,
	String name, Address address, int studentId,
	ArrayList<Subject> listSubject) {
		super(age, gender, name, address);
		this.studentId = studentId;
		this.listSubject = listSubject;
	}
	@Override
	public void eat() {
		System.out.println("Student is eating");
	}
	public void doHomework() {
		System.out.println("Student do homework");
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public ArrayList<Subject> getListSubject() {
		return listSubject;
	}
	public void setListSubject(ArrayList<Subject> listSubject) {
		this.listSubject = listSubject;
	}
}