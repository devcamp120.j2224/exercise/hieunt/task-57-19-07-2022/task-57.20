package com.task5720.abspersonclass;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
	@CrossOrigin
	@GetMapping("/listStudent")
	public ArrayList<Student> getStudentList() {
		// Tạo danh sách Student
		ArrayList<Student> studentList = new ArrayList<Student>();
		// Tạo danh sách subject 1
		ArrayList<Subject> subjectList1 = new ArrayList<Subject>();
		Subject subject01 = new Subject("Toan", 1, new Professor(50, "male", "Minh", new Address()));
		Subject subject02 = new Subject("Van", 2, new Professor(55, "female", "Huong", new Address(), 7000));
		subjectList1.add(subject01);
		subjectList1.add(subject02);
		// Tạo danh sách subject 2
		ArrayList<Subject> subjectList2 = new ArrayList<Subject>() {
			{
				add(new Subject("Vat Ly", 3, new Professor(45, "male", "Thang", new Address())));
				add(new Subject("Hoa Hoc", 4, new Professor(50, "female", "Thu", new Address(), 8000)));
			}
		};
		// Khởi tạo các đối tượng thuộc lớp Student
		Person student1 = new Student(18, "male", "Nam", new Address(), 1, subjectList1);
		Person student2 = new Student(19, "female", "Hoa", new Address(), 2, subjectList2);
		Person student3 = new Student(20, "female", "Lan", new Address(), 3,
			new ArrayList<Subject>(Arrays.asList(
				new Subject("Sinh Hoc", 5, new Professor(55, "male", "Trung", new Address())),
				new Subject("Lich Su", 6, new Professor(45, "female", "Trang", new Address()))
			))
		);
		// thêm các student và studentList	
		studentList.add((Student) student1);
		studentList.add((Student) student2);
		studentList.add((Student) student3);
		return studentList;
	}
}
