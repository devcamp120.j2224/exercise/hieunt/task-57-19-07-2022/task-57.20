package com.task5720.abspersonclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbspersonclassApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbspersonclassApplication.class, args);
	}

}
